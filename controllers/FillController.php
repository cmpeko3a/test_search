<?php

namespace controllers;

use models\base\App;
use models\base\Controller;
use models\fill\FillDatabase;

/**
 * Class FillController
 * @package controllers
 */
class FillController extends Controller
{
    public function actionDefault()
    {
        $model = new FillDatabase();

        return $this->render('index/fill', compact('model'));
    }

    public function actionInsert()
    {
        $model = new FillDatabase();
        if ($model->insert()) {
            App::$app->setFlash('info', 'Таблицы созданы и данные добавлены');
        }
        $this->redirect('/fill');
    }

    public function actionUpdate()
    {
        $model = new FillDatabase();
        if ($model->update()) {
            App::$app->setFlash('info', 'Данные обновлены');
        }
        $this->redirect('/fill');
    }

}