<?php

namespace controllers;

use models\base\App;
use models\base\Controller;
use models\fill\FillDatabase;
use models\search\SearchEmployee;

/**
 * Class SearchController
 * @package controllers
 */
class SearchController extends Controller
{
    public function actionDefault()
    {
        $searchModel = new SearchEmployee($_GET);
        if ($this->checkDb()){
            $searchModel->search();
        } else {
            $message = 'Для работы со справочником, необходимо выполнить установку таблиц.';
            return $this->render('search/no-db', compact('message'));
        }

        return $this->render('search/index', compact('searchModel'));
    }

    /**
     * @return bool
     */
    protected function checkDb()
    {
        $model = new FillDatabase();
        return $model->findTables();
    }
}