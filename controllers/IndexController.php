<?php

namespace controllers;

use models\base\Controller;

/**
 * Class IndexController
 * @package controllers
 */
class IndexController extends Controller
{
    public function actionDefault()
    {
        return $this->render('index/index');
    }
}