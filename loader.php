<?php
spl_autoload_register('autoLoader');

function autoLoader($className)
{
    $fileClass = str_replace('\\', DIRECTORY_SEPARATOR, $className);
    $fileName = __DIR__ . DIRECTORY_SEPARATOR . $fileClass . '.php';
    if (file_exists($fileName)) {
        require_once $fileName;
    }
}