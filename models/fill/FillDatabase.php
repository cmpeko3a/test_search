<?php

namespace models\fill;

use models\base\App;

/**
 * Class DbFill
 * @package classes
 */
class FillDatabase
{
    /**
     * @return bool
     * @throws \Exception
     */
    public function insert()
    {
        foreach ($this->getTablesList() as $table) {
            if (!$this->createTable($table)) {
                throw new \Exception('');
            }
        }

        return $this->update();
    }

    /**
     * @return bool
     * @throws \Exception
     */
    public function update()
    {
        try {
            $this->fillDepartment();
            $this->fillEmployee();
        } catch (\Exception $e) {
            throw new \Exception($e->getMessage());
        }

        return true;
    }

    /**
     * @return bool
     */
    public function findTables()
    {
        foreach ($this->getTablesList() as $table) {
            if (!$this->checkTable($table)) {
                return false;
            }
        }

        return true;
    }

    /**
     * @param string $table
     * @return bool
     */
    protected function checkTable($table)
    {
        $pdoSt = App::$app->getDb()->prepare('SHOW TABLES LIKE :table');
        $pdoSt->execute([':table' => $table]);

        return false !== $pdoSt->fetch(\PDO::FETCH_NUM);
    }

    /**
     * @param string $table
     * @return bool
     */
    protected function createTable($table)
    {
        if (!$fields = $this->getSchema($table)) {
            return $fields;
        }

        $sql = "CREATE TABLE IF NOT EXISTS `{$table}` (" . join(',', $fields) . ")";
        if (false === App::$app->getDb()->exec($sql)) {
            App::$app->getLog()->error("Ошибка создания таблицы `{$table}`");

            return false;
        }

        return true;
    }

    protected function fillDepartment()
    {
        App::$app->getDb()->exec('TRUNCATE TABLE  `Department`');
        $sql = "INSERT INTO `Department` (`Name`) VALUES (" . join('), (', array_fill(0, count(self::getDepartmentsNames()), '?')) . ")";
        $pdoSt = App::$app->getDb()->prepare($sql);
        if (!$pdoSt->execute(self::getDepartmentsNames())) {
            App::$app->getLog()->error("Ошибка добавления записей в таблицу `Department`");
        }
    }

    /**
     * @throws \Exception
     */
    protected function fillEmployee()
    {
        if (!$pdoSt = App::$app->getDb()->query('SELECT * FROM `Department`')) {
            throw new \Exception('Ошибка при обращение к таблице `Department`');
        }
        if (!$departments = $pdoSt->fetchAll(\PDO::FETCH_COLUMN)) {
            throw new \Exception('Ошибка таблица `Department` не содержит записей');
        }
        App::$app->getDb()->exec('TRUNCATE TABLE  `Employee`');

        $names = self::getEmployeeNames();
        $batchInsert = [];
        foreach ($names as $name) {
            list($firstName, $lastName, $middleName) = $name;
            $depId = $departments[rand(0, count($departments) - 1)];
            $batchInsert[] = [
                'FirstName' => $firstName,
                'LastName' => $lastName,
                'MiddleName' => $middleName,
                'DepartmentID' => $depId,
                'PhoneNumber' => rand(1000, 1050),
            ];
        }
        if ($batchInsert) {
            $keys = array_keys($batchInsert[0]);
            $values = array_fill(0, count($batchInsert), join(',', array_fill(0, count($keys), '?')));
            $sql = "INSERT INTO `Employee` (" . join(', ', $keys) . ") VALUES (" . join('), (', $values) . ")";
            $pdoSt = App::$app->getDb()->prepare($sql);
            $insertValues = [];
            foreach ($batchInsert as $data) {
                $insertValues = array_merge($insertValues, array_values($data));
            }
            if (!$pdoSt->execute($insertValues)) {
                App::$app->getLog()->error("Ошибка добавления записей в таблицу `Employee`");
            }
        }
    }

    /**
     * @return array
     */
    protected function getTablesList()
    {
        return [
            'Department',
            'Employee',
        ];
    }

    /**
     * @return array
     */
    protected function getTablesSchemas()
    {
        return [
            'Department' => [
                '`ID` INT(11) AUTO_INCREMENT PRIMARY KEY',
                '`Name` VARCHAR(100) NOT NULL',
            ],
            'Employee' => [
                '`ID` INT(11) AUTO_INCREMENT PRIMARY KEY',
                '`DepartmentID` INT(11) NOT NULL DEFAULT 0',
                '`FirstName` VARCHAR(100)',
                '`LastName` VARCHAR(100)',
                '`MiddleName` VARCHAR(100)',
                '`PhoneNumber` CHAR(4)',
                'KEY `DepartmentID` (`DepartmentID`)',
            ],
        ];
    }

    /**
     * @param $table
     * @return bool|mixed
     */
    protected function getSchema($table)
    {
        return isset($this->getTablesSchemas()[$table]) ? $this->getTablesSchemas()[$table] : false;
    }

    /**
     * @return array
     */
    protected static function getDepartmentsNames()
    {
        return [
            'Дирекция',
            'Продажи',
            'Маркетинг',
            'Логистика',
            'Склад',
            'Бухгалтерия',
            'Отдел персонала',
            'IT-отдел',
        ];
    }

    /**
     * @return array|bool
     */
    protected static function getEmployeeNames()
    {
        $post = [
            'fam' => 1,
            'imya' => 1,
            'otch' => 1,
            'pol' => 0,
            'count' => 300,
        ];
        $curl = curl_init();
        $curlParams = [
            CURLOPT_POST => true,
            CURLOPT_URL => 'http://freegenerator.ru/fio',
            CURLOPT_REFERER => 'http://freegenerator.ru/fio',
            CURLOPT_TIMEOUT => 5,
            CURLOPT_CONNECTTIMEOUT => 3,
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_FRESH_CONNECT => true,
            CURLOPT_FORBID_REUSE => true,
            CURLOPT_SSL_VERIFYHOST => false,
            CURLOPT_SSL_VERIFYPEER => false,
            CURLOPT_HTTPHEADER => ['Accept-Encoding: gzip'],
            CURLOPT_ENCODING => 'gzip',
        ];
        curl_setopt_array($curl, $curlParams);
        curl_setopt($curl, CURLOPT_POSTFIELDS, $post);

        if ($response = curl_exec($curl)) {
            if (!curl_errno($curl)) {
                $rows = array_filter(explode('<br>', $response), function ($value) {
                    return !empty(trim($value));
                });
                if (!empty($rows)) {
                    return array_map(function ($value) {
                        return explode(' ', trim($value));
                    }, $rows);
                }
            }
        }

        return false;
    }
}