<?php

namespace models\search;

use models\base\App;
use models\Department;
use models\Employee;

class SearchEmployee extends Employee
{
    public $page = 0;
    public $perPage = 15;
    public $query;

    public $dataCount;
    public $data;

    public function search()
    {
        $conditions = [
            'limit' => [
                'limit' => $this->perPage,
                'offset' => $this->perPage * $this->page,
            ]
        ];
        $bindParams = [];
        if ($this->query) {
            $conditions['where'] = [
                'FirstName LIKE :query',
                'LastName LIKE :query',
                'MiddleName LIKE :query',
                'PhoneNumber LIKE :query',
            ];

            $bindParams[':query'] = str_replace(['%', '_'], ['\%', '\_'], $this->query);

            $sql = 'SELECT id FROM ' . Department::getTableName() . ' WHERE Name LIKE :query';
            $pdoSt = App::$app->getDb()->prepare($sql);
            if ($pdoSt->execute($bindParams)) {
                if ($depId = $pdoSt->fetchColumn()) {
                    $conditions['where'][] = 'DepartmentID = :depId';
                    $bindParams[':depId'] = $depId;
                }
            }
        }

        if ($this->dataCount = $this->getCount($conditions, $bindParams)) {
            $this->data = $this->findAll($conditions, $bindParams);
            $this->populateRelation($this->data, 'department', Department::class, ['ID' => 'DepartmentID']);
        }
    }
}