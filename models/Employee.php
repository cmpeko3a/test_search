<?php

namespace models;

use models\base\ActiveModel;

/**
 * @property integer $ID
 * @property integer $DepartmentID
 * @property string $FirstName
 * @property string $LastName
 * @property string $MiddleName
 * @property string $PhoneNumber
 *
 * @property Department $department
 */
class Employee extends ActiveModel
{
    /**
     * @param integer $id
     */
    public function setId($id)
    {
        $this->ID = $id;
    }

    /**
     * @param integer $id
     */
    public function setDepartmentId($id)
    {
        $this->DepartmentID = $id;
    }

    /**
     * @param string $value
     */
    public function setFirstName($value)
    {
        $this->FirstName = $value;
    }

    /**
     * @param string $value
     */
    public function setLastName($value)
    {
        $this->LastName = $value;
    }

    /**
     * @param string $value
     */
    public function setMiddleName($value)
    {
        $this->MiddleName = $value;
    }

    /**
     * @param string $value
     */
    public function setPhoneNumber($value)
    {
        $this->PhoneNumber = $value;
    }

    /**
     * @param Department $value
     */
    public function setDepartment($value)
    {
        $this->department = $value;
    }

    /**
     * @inheritdoc
     */
    public function getTableName()
    {
        return 'Employee';
    }
}