<?php

namespace models;

use models\base\ActiveModel;

/**
 * @property integer $ID
 * @property string $Name
 */
class Department extends ActiveModel
{
    /**
     * @param int $ID
     */
    public function setID($ID)
    {
        $this->ID = $ID;
    }

    /**
     * @param string $Name
     */
    public function setName($Name)
    {
        $this->Name = $Name;
    }

    /**
     * @inheritdoc
     */
    public function getTableName()
    {
        return 'Department';
    }
}