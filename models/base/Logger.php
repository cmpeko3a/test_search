<?php

namespace models\base;

class Logger
{
    public static $path = '';

    public function __construct($config)
    {
        self::$path = $config['path'];
    }

    public static function getFilePath($level)
    {
        return self::$path . DIRECTORY_SEPARATOR . self::getFileName($level);
    }

    public function log($message, $level = 0)
    {
        return file_put_contents($this->getFilePath($level), self::logMessage($message), FILE_APPEND);
    }

    public function error($message)
    {
        return $this->log($message, 1);
    }

    public function exception($message)
    {
        return $this->log($message, 2);
    }

    public static function logMessage($message)
    {
        return str_pad(date('Y-m-d H:i:s'), 30, '=', STR_PAD_BOTH) . PHP_EOL . $message . PHP_EOL . str_repeat('=', 30) . PHP_EOL;
    }

    public static function getFileName($level)
    {
        $files = [
            0 => 'log.log',
            1 => 'error.log',
            2 => 'exc.log',
        ];

        return isset($files[$level]) ? $files[$level] : $files[0];
    }
}