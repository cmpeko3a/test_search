<?php

namespace models\base;

/**
 * Class Controller
 * @package models
 */
class Controller
{
    /**
     * @var string
    */
    public $layout = 'main';

    /**
     * @var string
     */
    public $defaultAction = 'actionDefault';

    /**
     * @var View
     */
    private $view;

    public function __construct()
    {
        $this->view = new View();
    }

    /**
     * @param string $route
     * @return mixed
     */
    public function run($route)
    {
        $methodName = $this->createAction($route);

        return call_user_func([$this, $methodName]);
    }

    /**
     * @param string $route
     * @return string
     */
    public function createAction($route)
    {
        $pos = strpos($route, '/');
        if ($pos === false) {
            $methodName = 'action' . str_replace(' ', '', ucwords(implode(' ', explode('-', $route))));
            if (method_exists($this, $methodName)) {
                return $methodName;
            }
        }

        return $this->defaultAction;
    }


    /**
     * @param string $view
     * @param array $params
     * @return string
     */
    public function render($view, $params = [])
    {
        $output = $this->view->render($view, $params);

        return $this->view->renderFile($this->findLayoutFile(), ['content' => $output]);
    }

    /**
     * @return string
     */
    public function findLayoutFile()
    {
        return $this->view->getViewsDir() . DIRECTORY_SEPARATOR . $this->layout . '.php';
    }

    /**
     * @param string $url
     * @param int $code
     */
    public function redirect($url, $code = 302)
    {
        header('Location: '. $url, true, $code);
        exit;
    }


}