<?php

namespace models\base;

/**
 * Class BaseModel
 * @package models\base
 */
abstract class ActiveModel extends Model
{

    /**
     * @return string
     */
    abstract public function getTableName();

    /**
     * @param array $condition
     * @param array $params
     * @return null|string
     */
    public function getCount($condition = [], $params = [])
    {
        $sql = 'SELECT COUNT(*) FROM ' . $this->getTableName();
        if (!empty($condition['where']) && is_array($condition['where'])) {
            $sql .= ' WHERE ' . join(' OR ', $condition['where']);
        }
        $pdoSt = App::$app->getDb()->prepare($sql);
        foreach ($params as $param => $value) {
            $pdoSt->bindValue($param, $value);
        }
        if ($pdoSt->execute()) {
            return $pdoSt->fetchColumn();
        }

        return null;
    }

    /**
     * @param $condition
     * @param array $params
     * @return array|null
     */
    public function findAll($condition, $params = [])
    {
        $sql = 'SELECT * FROM `' . $this->getTableName() . '`';
        if (!empty($condition['where']) && is_array($condition['where'])) {
            $sql .= ' WHERE ' . join(' OR ', $condition['where']);
        }

        if (!empty($condition['limit']) && is_array($condition['limit'])) {
            $limit = isset($condition['limit']['limit']) ? $condition['limit']['limit'] : 0;
            if (is_integer($limit) && $limit > 0) {
                $sql .= ' LIMIT ' . $limit;
            }
            $offset = isset($condition['limit']['offset']) ? $condition['limit']['offset'] : 0;
            if (is_integer($offset) and $offset > 0) {
                $sql .= ' OFFSET '. $offset;
            }
        }

        $pdoSt = App::$app->getDb()->prepare($sql);
        foreach ($params as $param => $value) {
            $pdoSt->bindValue($param, $value);
        }
        if ($pdoSt->execute()) {
            return $pdoSt->fetchAll(\PDO::FETCH_CLASS, static::class);
        }

        return null;
    }

    /**
     * @param ActiveModel[] $models
     * @param string $relationName
     * @param string $className
     * @param array $relation
     */
    public function populateRelation(&$models, $relationName, $className, $relation)
    {
        $classKey = key($relation);
        $modelKey = current($relation);
        $class = new $className();
        /* @var ActiveModel $class */
        $onIds = array_unique(array_map(function ($model) use ($modelKey) {return (int)$model->$modelKey;}, $models));
        if ($onIds) {
            $sql = "SELECT * FROM `" . $class->getTableName() . "` WHERE $classKey IN (" . join(', ', $onIds) . ")";
            $pdoSt = App::$app->getDb()->query($sql);
            $relatedModels = $pdoSt->fetchAll(\PDO::FETCH_CLASS, $class::className());
            $relatedKeys = array_map(function ($model) use ($classKey) { return $model->$classKey;}, $relatedModels);
            array_walk($models, function ($data) use ($relatedModels, $relatedKeys, $relationName) {
                if ($data->DepartmentID) {
                    $key = array_search($data->DepartmentID, $relatedKeys);
                    if ($key !== false) {
                        $data->$relationName = $relatedModels[$key];
                    }
                }
            });
        }
    }
}