<?php

namespace models\base;

class Model
{
    /**
     * @param array $params
     */
    function __construct($params = [])
    {
        if (!empty($params)) {
            foreach ($params as $name => $value) {
                $this->$name = $value;
            }
        }
    }

    /**
     * @param string $name
     * @return mixed
     * @throws \Exception
     */
    public function __get($name)
    {
        $getter = 'get' . $name;
        if (method_exists($this, $getter)) {
            return $this->$getter();
        } elseif (method_exists($this, 'set' . $name)) {
            throw new \Exception('Получение write-only свойства: ' . get_class($this) . '::' . $name);
        } else {
            throw new \Exception('Неизвестное свойсто: ' . get_class($this) . '::' . $name);
        }
    }

    /**
     * @param string $name
     * @param mixed $value
     * @throws \Exception
     */
    public function __set($name, $value)
    {
        $setter = 'set' . $name;
        if (method_exists($this, $setter)) {
            $this->$setter($value);
        } elseif (method_exists($this, 'get' . $name)) {
            throw new \Exception('Установка read-only свойства: ' . get_class($this) . '::' . $name);
        } else {
            throw new \Exception('Неизвестное свойство: ' . get_class($this) . '::' . $name);
        }
    }

    /**
     * @param string $name
     * @return boolean
     */
    public function __isset($name)
    {
        $getter = 'get' . $name;
        if (method_exists($this, $getter)) {
            return $this->$getter() !== null;
        } else {
            return false;
        }
    }

    /**
     * @param string $name
     * @throws \Exception
     */
    public function __unset($name)
    {
        $setter = 'set' . $name;
        if (method_exists($this, $setter)) {
            $this->$setter(null);
        } elseif (method_exists($this, 'get' . $name)) {
            throw new \Exception('Unsetting read-only property: ' . get_class($this) . '::' . $name);
        }
    }

    public function load($data)
    {
        if (!empty($data)) {
            $this->setAttributes($data);

            return true;
        }

        return false;
    }

    /**
     * @param array $values
     */
    public function setAttributes($values)
    {
        if (is_array($values)) {
            $attributes = array_flip($this->attributes());
            foreach ($values as $name => $value) {
                if (isset($attributes[$name])) {
                    $this->$name = $value;
                }
            }
        }
    }

    /**
     * @return string
     */
    public static function className()
    {
        return get_called_class();
    }

    /**
     * @return string
     */
    public function formName()
    {
        $reflector = new \ReflectionClass($this);

        return $reflector->getShortName();
    }

    /**
     * @return array
     */
    public function attributes()
    {
        $class = new \ReflectionClass($this);
        $names = [];
        foreach ($class->getProperties(\ReflectionProperty::IS_PUBLIC) as $property) {
            if (!$property->isStatic()) {
                $names[] = $property->getName();
            }
        }

        return $names;
    }

}