<?php

namespace models\base;

class View {

    /**
     * @var string
     */
    public $title;

    /**
     * @param string $view
     * @param array $params
     * @return string
     */
    public function render($view, $params = [])
    {
        $viewFile = $this->findViewFile($view);

        return $this->renderFile($viewFile, $params);
    }

    /**
     * @param string $view
     * @return string
     */
    protected function findViewFile($view)
    {
        $file = $this->getViewsDir() . DIRECTORY_SEPARATOR . ltrim($view, '/');
        if (pathinfo($file, PATHINFO_EXTENSION) !== '') {
            return $file;
        }
        $path = $file . '.php';

        return $path;
    }

    /**
     * @param $viewFile
     * @param array $params
     * @return string
     * @throws \Exception
     */
    public function renderFile($viewFile, $params = [])
    {
        if (!is_file($viewFile)) {
            throw new \Exception("Файл не найден: $viewFile");
        }
        ob_start();
        ob_implicit_flush(false);
        extract($params, EXTR_OVERWRITE);
        require($viewFile);

        return ob_get_clean();
    }

    /**
     * @return string
     */
    public function getViewsDir()
    {
        return App::$app->getConfig()['basePath'] . DIRECTORY_SEPARATOR . 'views';
    }
}