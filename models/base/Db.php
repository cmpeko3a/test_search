<?php

namespace models\base;

/**
 * Class Db
 * @package models\base
 */
class Db extends \PDO
{
    /**
     * @var \PDO
     */
    public static $conn;

    /**
     * Db constructor.
     * @param array $config
     */
    public function __construct($config)
    {
        parent::__construct($config['dsn'], $config['username'], $config['password'], $config['options']);
    }
}