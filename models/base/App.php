<?php

namespace models\base;

class App
{

    public $controllerNs = 'controllers';

    public $defaultController = 'index';

    /**
     * @var Controller
     */
    public $controller;
    /**
     * @var self
     */
    public static $app;
    /**
     * @var array
     */
    private static $config;
    /**
     * @var Db
     */
    private static $db;
    /**
     * @var Logger
     */
    private static $log;

    /**
     * @param array $config
     */
    public function __construct($config)
    {
        self::$app = $this;
        self::$config = $config;
        $this->init();
    }

    public function init()
    {
        if (isset(self::$config['db'])) {
            self::$db = new Db(self::$config['db']);
        }
        if (isset(self::$config['log'])) {
            self::$log = new Logger(self::$config['log']);
        }
    }

    /**
     * @param string $route
     * @return mixed
     */
    public function run($route)
    {
        list($controller, $action) = $this->parseRoute($route);

        if (!class_exists($controller)) {
            //self::$log->exception('Bad controllerName "' . $controller . '"');
            $controller = $this->createController($this->defaultController);
        }
        $this->controller = new $controller();

        return $this->controller->run($action);
    }

    /**
     * @param string $route
     * @return array
     */
    public function parseRoute($route)
    {
        $route = trim($route, '/');
        if (strpos($route, '/') !== false) {
            list($controller, $route) = explode('/', $route, 2);
        } else {
            $controller = $route;
            $route = '';
        }
        $controller = $this->createController($controller);
        $action = $this->createAction($route);

        return [$controller, $action];
    }

    /**
     * @param string $route
     * @return string
     */
    public function createController($route)
    {
        $className = $route;
        $pos = strrpos($route, '/');
        if ($pos !== false) {
            $className = substr($route, $pos + 1);
        }
        $className = str_replace(' ', '', ucwords(str_replace('-', ' ', $className))) . 'Controller';
        $className = ltrim($this->controllerNs . '\\' . $className, '\\');

        return $className;
    }

    /**
     * @param string $route
     * @return string
     */
    public function createAction($route)
    {
        $actionId = $route;
        $pos = strpos(trim($route, '/'), '/');
        if ($pos !== false) {
            $actionId = substr($route, 0, $pos);
        }

        return $actionId;
    }

    /**
     * @return Db
     */
    public function getDb()
    {
        return self::$db;
    }

    /**
     * @return Logger
     */
    public function getLog()
    {
        return self::$log;
    }

    /**
     * @return array
     */
    public function getConfig()
    {
        return self::$config;
    }

    /**
     * @param string $key
     * @param bool $value
     */
    public function setFlash($key, $value = true)
    {
        $_SESSION[$key] = $value;
    }

    /**
     * @param string $key
     * @param null $default
     * @return mixed
     */
    public function getFlash($key, $default = null)
    {
        if (isset($_SESSION[$key])) {
            $value = $_SESSION[$key];
            unset($_SESSION[$key]);

            return $value;
        }

        return $default;
    }
}