<?php
require __DIR__ . '/loader.php';
$config = require __DIR__ . '/config/app.php';
session_start();
$app = new \models\base\App($config);
$route = isset($_SERVER['REQUEST_URI']) ? parse_url($_SERVER['REQUEST_URI'], PHP_URL_PATH) : '';
echo $app->run($route);