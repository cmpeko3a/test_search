<?php
use models\base\App;

/* @var $this \models\base\View */
/* @var $content string */

$navs = [
    '/' => 'Главная',
    '/fill' => 'Заполнить БД',
    '/search' => 'Поиск',
];
?>
<!DOCTYPE html>
<html lang="ru">
<head>
    <meta charset="utf-8"/>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title><?= htmlspecialchars($this->title) ?></title>
    <link href="/assets/css/bootstrap.min.css" rel="stylesheet">
</head>
<body>
    <header>
        <p></p>
    </header>
    <div class="container">
        <ul class="nav nav-pills">
            <?php foreach ($navs as $key => $value) { ?>
                <li<?= $key == $_SERVER['REQUEST_URI'] ? ' class="active"' : '' ?>>
                    <a href="<?= $key ?>"><?= $value ?></a>
                </li>
            <?php } ?>
        </ul>
        <h1><?= $this->title ?></h1>
        <?php if ($flash = App::$app->getFlash('info')) { ?>
            <div class="alert alert-info" role="alert"><?= $flash ?></div>
        <?php } ?>
        <?= $content ?>
    </div>
    <div class="footer">
        <p></p>
    </div>
</body>
</html>