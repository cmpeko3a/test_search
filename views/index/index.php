<?php
/* @var $this \models\base\View */
$this->title = 'Постановка задачи';
?>
<p>Требуется сделать простой телефонный справочник, интерфейс которого на веб-странице состоит из формы, содержащей одно текстовое поле. В данное текстовое поле может
    быть введена ФИО, либо телефонный номер, либо название отдела.</p>
<p>В качестве хранилища данных используется следующая структура таблиц:</p>
<strong>Department</strong>
<table class="table table-bordered">
    <colgroup>
        <col width="15%"/>
        <col width="15%"/>
        <col width="70%"/>
    </colgroup>
    <thead>
    <tr>
        <th>Атрибут</th>
        <th>Тип</th>
        <th>Комментарий</th>
    </tr>
    </thead>
    <tbody>
    <tr>
        <td>ID</td>
        <td>int</td>
        <td>Идентификатор записи</td>
    </tr>
    <tr>
        <td>Name</td>
        <td>varchar(100)</td>
        <td>Наименование отдела</td>
    </tr>
    </tbody>
</table>
<strong>Employee</strong>
<table class="table table-bordered">
    <colgroup>
        <col width="15%"/>
        <col width="15%"/>
        <col width="70%"/>
    </colgroup>
    <thead>
    <tr>
        <th>Атрибут</th>
        <th>Тип</th>
        <th>Комментарий</th>
    </tr>
    </thead>
    <tbody>
    <tr>
        <td>ID</td>
        <td>int</td>
        <td>Идентификатор записи</td>
    </tr>
    <tr>
        <td>DepartmentID</td>
        <td>int</td>
        <td>Идентификатор записи отдела</td>
    </tr>
    <tr>
        <td>FirstName</td>
        <td>varchar(100)</td>
        <td>Имя</td>
    </tr>
    <tr>
        <td>LastName</td>
        <td>varchar(100)</td>
        <td>Фамилия</td>
    </tr>
    <tr>
        <td>MiddleName</td>
        <td>varchar(100)</td>
        <td>Отчество</td>
    </tr>
    <tr>
        <td>PhoneNumber</td>
        <td>char(4)</td>
        <td>Номер телефона (четырехзначный)</td>
    </tr>
    </tbody>
</table>
<p>Результатом отправки запроса из формы должно являться в зависимости от того, какие данные введены: найденные записи сотрудников по ФИО или по номеру телефона, либо
    списочный состав отдела.</p>
<h2>Требования к реализации</h2>
<p>СУБД - MySQL, файл данных должен быть приложен в качестве SQL-дампа.</p>