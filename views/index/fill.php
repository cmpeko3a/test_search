<?php
/* @var $this \models\base\View */
/* @var $model \models\fill\FillDatabase */

$this->title = 'Заполнить БД тестовыми значениями';
$tableOk = $model->findTables();
$buttonValue = $tableOk ? 'Заменить данные в таблицах' : 'Создать таблицы и заполнить';
?>
<form action="/fill/<?= $tableOk ? 'update' : 'insert'?>" method="post" class="form-inline">
    <button type="submit" class="btn btn-primary"><?= $buttonValue?></button>
</form>