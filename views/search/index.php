<?php
/* @var $this \models\base\View */
/* @var $searchModel \models\search\SearchEmployee */
$this->title = 'Телефонный справочник';
?>
<form action="" class="form-inline">
    <div class="form-group">
        <label for="inputQuery">Что ищем</label>
        <input type="text" id="inputQuery" name="query" class="form-control"
               placeholder="ФИО, телефонный номер, название отдела"
               value="<?= htmlspecialchars($searchModel->query) ?>">
    </div>
    <button type="submit" class="btn btn-default">Найти</button>
</form>
<br/>
<?php if ($searchModel->query) { ?>
    <p>
        По вашему запросу "<?= htmlspecialchars($searchModel->query) ?>"
        <?= $searchModel->dataCount ? 'нашли' : 'ничего не найдено' ?>
    </p>
<?php } ?>
<?php if ($searchModel->data) { ?>
    <table class="table table-bordered">
        <thead>
            <tr>
                <th>#</th>
                <th>Имя</th>
                <th>Фамилия</th>
                <th>Отчество</th>
                <th>Телефон</th>
                <th>Отдел</th>
            </tr>
        </thead>
        <tbody>
        <?php $counter = 1;
        foreach ($searchModel->data as $dataSearch) {
            /* @var $dataSearch \models\Employee */ ?>
            <tr>
                <td><?= $counter++ ?></td>
                <td><?= $dataSearch->FirstName ?></td>
                <td><?= $dataSearch->LastName ?></td>
                <td><?= $dataSearch->MiddleName ?></td>
                <td><?= $dataSearch->PhoneNumber ?></td>
                <td><?= $dataSearch->department->Name ?></td>
            </tr>
        <?php } ?>
        </tbody>
    </table>
<?php } ?>
<?php if ($searchModel->dataCount > $searchModel->perPage) {
    $pageSize = ceil($searchModel->dataCount / $searchModel->perPage); ?>
    <ul class="pagination">
        <?php for ($page = 0; $page < $pageSize; $page++) { ?>
            <li<?= $page == $searchModel->page ? ' class="active"' : '' ?>>
                <a href="/search?<?= http_build_query(['query' => $searchModel->query, 'page' => $page]) ?>"><?= $page + 1 ?></a>
            </li>
        <? } ?>
    </ul>
<?php } ?>
