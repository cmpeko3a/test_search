<?php

$config = [
    'basePath' => dirname(__DIR__),
    'siteUrl' => 'http://www.example.com',
    'log' => [
        'path' => dirname(__DIR__). '/runtime',
    ],
    'db' => [
        'dsn' => 'mysql:host=localhost;dbname=;charset=utf8',
        'username' => '',
        'password' => '',
        'options' => [],
    ],
];

return $config;