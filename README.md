# Телефонный справочник

В корне проекта размещен файл-дампа БД, так же возможно выполнить установку необходимых таблиц и заполнить их, используя внутреннюю команду.

Для работы необходимо скопировать файл config/sample.app.php в config/app.php и отредактировать содержимое подключения к БД(имя БД, пользователь и пароль).

Необходимо настроить веб сервер чтоб все запросы передавались на файл index.php расположенный в корне проекта. 

Пример минимальной настроики проекта для web-сервера nginx
```
server {
    server_name test.ru;
    root /var/www/test.ru;
    access_log /var/log/nginx/test.ru.access.log;
    error_log /var/log/nginx/test.ru.error.log;
    
    index index.php index.html;
    
    location / {
        try_files $uri $uri/ /index.php?$args;# =404;
    }
    
    location ~ \.php$ {
        fastcgi_pass unix:/var/run/php5-fpm.sock;
        fastcgi_index index.php;
        include fastcgi_params;
    }
    
    location ~ /\. {
        deny all;
    }
}
```

